#!/bin/bash
CONTAINERS="chuck_1 chuck_2 chuck_3"
  for CONTAINER in ${CONTAINERS}
  do
    RUNNING=$(docker inspect  --format="{{ .State.Running }}" $CONTAINER)

      if [ "$RUNNING" != "true" ]; then
              echo "$CONTAINER is down!!"
      fi
  done
